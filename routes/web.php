<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () { return view('custom.login'); });
Route::get('/signup', function () { return view('custom.signup'); });

Auth::routes();

Route::group(['middleware' => ['auth']], function () {
    Route::post('/update-collapse', 'HomeController@Update_Collapse');
    
    Route::get('/home', 'HomeController@index')->name('home');
    Route::get('/home-superadmin', 'admin\C_Home@index')->name('home-superadmin');

    //Organization
    Route::get('/registration-org', 'Organization\C_Organization@index')->name('registration-org');
});
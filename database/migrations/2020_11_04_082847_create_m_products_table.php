<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('m_products', function (Blueprint $table) {
            $table->bigIncrements('m_product_id');
            $table->timestamps();
            $table->bigInteger('created_by');
            $table->bigInteger('updated_by');
            $table->boolean('is_active')->default(true);
            $table->string('value',100);
            $table->string('nama',100);
            $table->string('description',250)->nullable();
            $table->bigInteger('organization_id');
            $table->string('product_category',100)->nullable();
            $table->float('buyprice',50,4)->nullable();
            $table->float('sellprice',50,4);
            $table->string('uom',50)->nullable();
            $table->string('pciature',100)->nullable();

            $table->foreign('organization_id')->references('organization_id')->on('organizations');
            $table->foreign('created_by')->references('id')->on('users');
            $table->foreign('updated_by')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('m_products');
    }
}

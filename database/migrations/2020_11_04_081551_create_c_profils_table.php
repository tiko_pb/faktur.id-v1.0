<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCProfilsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('c_profils', function (Blueprint $table) {
            $table->bigIncrements('c_profil_Id');
            $table->timestamps();
            $table->bigInteger('created_by');
            $table->bigInteger('updated_by');
            $table->boolean('is_active')->default(true);
            $table->bigInteger('organization_id')->nullable();
            $table->string('value',100);
            $table->string('nama',100);
            $table->string('description',250)->nullable();
            $table->bigInteger('c_profil_category_id');
            $table->string('usaha_type',100)->nullable();
            $table->string('badan_usaha',100)->nullable();
            $table->string('address',100);
            $table->string('email',100);
            $table->string('bodyemail',250)->nullable();
            $table->string('footyemail',250)->nullable();
            $table->string('phonenumber1',50);
            $table->string('phonenumber2',50)->nullable();
            $table->string('website',100)->nullable();
            $table->string('npwp',75)->nullable();
            $table->string('logo',100)->nullable();
            $table->string('signature',100)->nullable();
            $table->string('namatertera',100)->nullable();

            $table->foreign('organization_id')->references('organization_id')->on('organizations');
            $table->foreign('created_by')->references('id')->on('users');
            $table->foreign('updated_by')->references('id')->on('users');
            $table->foreign('c_profil_category_id')->references('c_profil_category_id')->on('c_profil_categorys');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('c_profils');
    }
}

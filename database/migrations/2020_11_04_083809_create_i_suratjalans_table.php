<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateISuratjalansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('i_suratjalans', function (Blueprint $table) {
            $table->bigIncrements('i_suratjalan_id');
            $table->timestamps();
            $table->bigInteger('created_by');
            $table->bigInteger('updated_by');
            $table->boolean('is_active')->default(true);
            $table->string('documentno',100);
            $table->string('description',250)->nullable();
            $table->bigInteger('organization_id');
            $table->bigInteger('c_partner_id');
            $table->bigInteger('m_product_id');
            $table->float('qty', 50, 4);
            $table->string('uom',50);
            $table->float('weight', 50, 4);
            $table->float('totalkoli', 50, 4);

            $table->foreign('organization_id')->references('organization_id')->on('organizations');
            $table->foreign('created_by')->references('id')->on('users');
            $table->foreign('updated_by')->references('id')->on('users');
            $table->foreign('c_partner_id')->references('c_partner_id')->on('c_partners');
            $table->foreign('m_product_id')->references('m_product_id')->on('m_products');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('i_suratjalans');
    }
}

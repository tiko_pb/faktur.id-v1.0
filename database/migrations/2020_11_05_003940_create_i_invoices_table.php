<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIInvoicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('i_invoices', function (Blueprint $table) {
            $table->bigIncrements('i_invoice_id');
            $table->timestamps();
            $table->bigInteger('created_by');
            $table->bigInteger('updated_by');
            $table->boolean('is_active')->default(true);
            $table->string('description',250)->nullable();
            $table->bigInteger('organization_id');
            $table->string('documentno',100);
            $table->date('invoiceDate');
            $table->string('termPayment',100);
            $table->date('dueDate');
            $table->bigInteger('c_partner_id');
            $table->string('salesPerson',100)->nullable();
            $table->boolean('idPaid')->default(true);
            $table->float('amountinvoiced')->default(0);
            
            $table->foreign('organization_id')->references('organization_id')->on('organizations');
            $table->foreign('created_by')->references('id')->on('users');
            $table->foreign('updated_by')->references('id')->on('users');
            $table->foreign('c_partner_id')->references('c_partner_id')->on('c_partners');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('i_invoices');
    }
}

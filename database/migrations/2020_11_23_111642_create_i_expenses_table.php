<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIExpensesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('i_expenses', function (Blueprint $table) {
            $table->bigIncrements('i_expense_id');
            $table->timestamps();
            $table->bigInteger('created_by');
            $table->bigInteger('updated_by');
            $table->boolean('is_active')->default(true);
            $table->string('description',250)->nullable();
            $table->bigInteger('organization_id');
            $table->string('documentno',100);
            $table->integer('expence_type');
            $table->string('expence_account',100);
            $table->bigInteger('c_partner_id');
            $table->date('expence_date');
            $table->float('amount',4,2);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('i_expenses');
    }
}

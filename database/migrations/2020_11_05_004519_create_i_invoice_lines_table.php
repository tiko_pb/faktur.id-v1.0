<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIInvoiceLinesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('i_invoice_lines', function (Blueprint $table) {
            $table->bigIncrements('i_invoice_line_id');
            $table->timestamps();
            $table->bigInteger('created_by');
            $table->bigInteger('updated_by');
            $table->boolean('is_active')->default(true);
            $table->string('value',100);
            $table->string('nama',100);
            $table->string('description',250)->nullable();
            $table->bigInteger('organization_id');
            $table->bigInteger('m_product_id');
            $table->float('qty',50,4);
            $table->string('uom');
            $table->float('price',50,4);
            $table->float('pajak',50,4);
            $table->float('discount',50,4);
            $table->float('pricetotal',50,4);
            $table->bigInteger('i_invoice_id');

            $table->foreign('organization_id')->references('organization_id')->on('organizations');
            $table->foreign('created_by')->references('id')->on('users');
            $table->foreign('updated_by')->references('id')->on('users');
            $table->foreign('i_invoice_id')->references('i_invoice_id')->on('i_invoices');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('i_invoice_lines');
    }
}

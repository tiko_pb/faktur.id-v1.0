<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCPartnersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('c_partners', function (Blueprint $table) {
            $table->bigIncrements('c_partner_id');
            $table->timestamps();
            $table->bigInteger('created_by');
            $table->bigInteger('updated_by');
            $table->boolean('is_active')->default(true);
            $table->string('value',100);
            $table->string('nama',100);
            $table->string('description',250)->nullable();
            $table->bigInteger('organization_id');
            $table->string('badan_usaha',100)->nullable();
            $table->string('address',100)->nullable();
            $table->string('email',100)->nullable();
            $table->string('phonenumber1',50)->nullable();
            $table->string('phonenumber2',50)->nullable();
            $table->string('partner_type',75);
            $table->string('address_line1',150);
            $table->string('address_line2',150)->nullable();
            $table->string('postal_code varchar',25);
            $table->string('city',50);
            $table->string('province',50);
            $table->string('npwp',50);
            $table->boolean('kaber')->default(false);
            $table->foreign('organization_id')->references('organization_id')->on('organizations');
            $table->foreign('created_by')->references('id')->on('users');
            $table->foreign('updated_by')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('c_partners');
    }
}

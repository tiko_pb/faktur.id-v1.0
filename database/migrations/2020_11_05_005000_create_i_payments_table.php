<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIPaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('i_payments', function (Blueprint $table) {
            $table->bigIncrements('i_payment_id');
            $table->timestamps();
            $table->bigInteger('created_by');
            $table->bigInteger('updated_by');
            $table->boolean('is_active')->default(true);
            $table->string('description',250)->nullable();
            $table->bigInteger('organization_id');
            $table->string('documentno',100);
            $table->string('paymentAccount',100);
            $table->date('paymentDate');
            $table->bigInteger('i_invoice_id');
            $table->string('paymentMethod',100);
            $table->float('totalPayment',50,4);
            $table->float('totalPaymentPaid',50,4);
            $table->string('file',100);
            $table->string('docstatus',100);

            $table->foreign('organization_id')->references('organization_id')->on('organizations');
            $table->foreign('created_by')->references('id')->on('users');
            $table->foreign('updated_by')->references('id')->on('users');
            $table->foreign('i_invoice_id')->references('i_invoice_id')->on('i_invoices');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('i_payments');
    }
}

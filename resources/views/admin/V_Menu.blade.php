      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
          <li class="nav-item">
            <a href="/home-superadmin" class="nav-link">
              <i class="nav-icon fas fa-home"></i>
              <p>
                Dashboard
              </p>
            </a>
          </li>

          {{--partner  start--}}
          <li class="nav-item">
            <a href="/home-superadmin" class="nav-link">
              <i class="nav-icon fas fa-address-card"></i>
              <p>
                Partner
              </p>
            </a>
          </li>
          {{--partner end--}}

          {{--Order  start--}}
          <li class="nav-item">
            <a href="/home-superadmin" class="nav-link">
              <i class="nav-icon fas fa-paper-plane"></i>
              <p>
                Order
              </p>
            </a>
          </li>
          {{--Order end--}}

          {{--invoice  start--}}
          <li class="nav-item">
            <a href="/home-superadmin" class="nav-link">
              <i class="nav-icon fas fa-list-alt"></i>
              <p>
                Invoice
              </p>
            </a>
          </li>
          {{--Invoice  end--}}

          {{--product  start--}}
          <li class="nav-item">
            <a href="/home-superadmin" class="nav-link">
              <i class="nav-icon fas far fa-folder-open"></i>
              <p>
                Product
              </p>
            </a>
          </li>
          {{--product  end--}}
      
          {{--Cost  start--}}
          <li class="nav-item">
            <a href="/home-superadmin" class="nav-link">
              <i class="nav-icon fas fa-money-bill-wave"></i>
              <p>
                Cost
              </p>
            </a>
          </li>
          {{--Cost  end--}}

          {{--Report  start--}}
          <li class="nav-item">
            <a href="/home-superadmin" class="nav-link">
              <i class="nav-icon fas fa-chart-line"></i>
              <p>
                Report
              </p>
            </a>
          </li>
          {{--Report  end--}}

          {{--FAQ  start--}}
          <li class="nav-item">
            <a href="/home-superadmin" class="nav-link">
              <i class="nav-icon fas fa-question-circle"></i>
              <p>
                Faq
              </p>
            </a>
          </li>
          {{--FAQ  end--}}

        </ul>
      </nav>
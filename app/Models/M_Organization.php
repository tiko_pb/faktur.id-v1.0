<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class M_Organization extends Model
{
    protected $table = 'organizations';
    public $incrementing = false;
    protected $guarded = ['organization_id'];
    protected $fillable = ['name','email','is_active','description'];
    protected $dates = ['created_at','deleted_at'];
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class M_User extends Model
{
    protected $table = 'users';
    public $incrementing = false;
    protected $guarded = ['id'];
    protected $fillable = ['name','email','email_verified_at','password','sidebar','organization_id','role_id'];
    protected $dates = ['created_at','deleted_at'];
}

<?php

namespace App\Http\Controllers\Organization;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\User;
use App\Models\M_User;

class C_Organization extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('Organization.registrationOrg');
    }

    public function Myprofil()
    {
        $User = new User();
        $id = Auth::id();
        $where = array('id' => $id);
        return $User->Myprofil_Process($where)->first();
    }

    public function Update_Collapse(Request $Request)
    {
        $User = new User();
        $id = $Request->input('id');
        $a = $this->Myprofil()->sidebar;
        if($a == 'sidebar-collapse'){
            $data = array('sidebar' => '' );
			$where = array('id' => $id);
            $User->Update_Data('users', $where, $data);
        }else{
            $data = array('sidebar' => 'sidebar-collapse');
            $where = array('id' => $id);
            $User->Update_Data('users', $where, $data);
        }
    }
}

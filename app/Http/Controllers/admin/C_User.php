<?php

namespace App\Http\Controllers\wepos;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\HomeController;
use App\Models\wepos\M_User;
use App\Models\wepos\M_OrganizationAndClient;
use App\Models\wepos\M_AppAndRole;

class C_User extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $M_User = new M_User();
        $HomeController = new HomeController();
        $user_name = Auth::user()->name;
        $id = Auth::id();
        $myprofil = $HomeController->MyProfil()->sidebar;

        $M_OrganizationAndClient = new M_OrganizationAndClient();
        $M_AppAndRole = new M_AppAndRole();

        $where1 = array('is_active'=>'t');
        $d_app = $M_AppAndRole->view_data('w_apps', $where1)->get();
        $d_role = $M_AppAndRole->view_data('w_roles', $where1)->get();
        $d_organization = $M_OrganizationAndClient->view_data('w_organizations', $where1)->get();
        $d_client = $M_OrganizationAndClient->view_data('w_clients', $where1)->get();
        
        $data = array(
            'id' => $id,
            'user_name' => $user_name,
            'sidebar' => $myprofil,
            'data_app' => $d_app,
            'data_role' => $d_role,
            'data_organization' => $d_organization,
            'data_client' => $d_client
        );
        return view('wepos.V_User', $data);
    }
    
}
